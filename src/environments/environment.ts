// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


export const environment = {
  production: false,
  url:' http://localhost/angular/slim/',
  firebase:{
   apiKey: "AIzaSyBAPrCMOJSm6gA4qNIP81tMtLZIwN2mpjo",
    authDomain: "users-87a44.firebaseapp.com",
    databaseURL: "https://users-87a44.firebaseio.com",
    projectId: "users-87a44",
    storageBucket: "users-87a44.appspot.com",
    messagingSenderId: "794895010928"
     }
 
};