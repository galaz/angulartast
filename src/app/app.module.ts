import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UsersComponent } from './users/users.component';
import { CarsComponent } from './cars/cars.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersService } from './users/users.service';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { UpdateFormComponent } from './users/update-form/update-form.component';
import { UsersFirebaseComponent } from './users/users-firebase/users-firebase.component';

import { UsersNavigationComponent } from './users/users-navigation/users-navigation.component';
import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { FproductsComponent } from './products/fproducts/fproducts.component';
import { ProductsNavigationComponent } from './products/products-navigation/products-navigation.component';
import { ProductFormComponent } from './products/product-form/product-form.component';
import { SearchResultsComponent } from './products/search-results/search-results.component';

const appRoutes: Routes = [
  { path: ''                    , component: ProductsComponent           },
  { path: 'users'               , component: UsersComponent              },
  { path: 'products'             , component: ProductsComponent          },
  { path: 'edit-product/:id'     , component: EditProductComponent       },
 // { path: 'users-firebase'      , component: UsersFirebaseComponent      },
  { path: 'search-results/:search_id'  , component: SearchResultsComponent  },
  { path: 'cars'                , component: CarsComponent               },
  { path: 'update-form/:id'     , component: UpdateFormComponent         },
  { path: 'fproducts'           , component: FproductsComponent           },
  {path: 'login'                 , component: LoginComponent              },
  { path: '**'                  , component: NotFoundComponent           }  
];

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    UsersComponent,
    CarsComponent,
    NavigationComponent,
    UsersFormComponent,
    UpdateFormComponent,
    UsersFirebaseComponent,
    UsersNavigationComponent,
    LoginComponent,
    ProductsComponent,
    EditProductComponent,
    FproductsComponent ,
    ProductsNavigationComponent,
    ProductFormComponent,
    SearchResultsComponent
 
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(      
      appRoutes,
      { enableTracing: false }
    )
  ],
  providers: [UsersService,ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }