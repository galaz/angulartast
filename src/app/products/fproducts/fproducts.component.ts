import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { AngularFireDatabase } from 'angularfire2/database';
@Component({
  selector: 'app-fproducts',
  templateUrl: './fproducts.component.html',
  styleUrls: ['./fproducts.component.css']
})
export class FproductsComponent implements OnInit {

   // Variables	  
  products;
  productsKeys = [];

  constructor(private service:ProductsService) {  	
  	service.getProductsFireBase().subscribe(response=>{       		
    	this.productsKeys = Object.values(response);    	
 	  });
  }
  ngOnInit() {
  }

}
