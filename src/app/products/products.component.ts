import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ProductsService } from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {


  
  length;
  
  // Variables	
  products;
  productsKeys = [];
  //Form Builder
  searchF = new FormGroup({
      name:new FormControl()
  }); 

   constructor(private service:ProductsService, private router:Router, private formBuilder:FormBuilder) {  	
  	
    service.getProducts().subscribe(response=>{      
    	this.products = response.json();
      this.productsKeys = Object.keys(this.products);
      this.length = this.productsKeys.length;
      console.log(this.productsKeys)
 	  });         
 	
  }

  deleteProduct(key){    
    let index = this.productsKeys.indexOf(key);
    this.productsKeys.splice(index,1);
    this.service.deleteProduct(key).subscribe(
      response=> console.log(response));
  }

  updateProduct(id){
    this.service.getProduct(id).subscribe(response=>{
      this.products = response.json();      
    });   
  }

  sendData(){
    this.router.navigate(['/search-results/' + this.searchF.value.name]);
  }
  ngOnInit() {
  }

}
