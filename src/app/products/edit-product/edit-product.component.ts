import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
name;
price;

  // Adding emitters
  @Output() addProduct:EventEmitter <any> = new EventEmitter <any>();
  @Output() addProductPs:EventEmitter <any> = new EventEmitter <any>();
  // Instance Variables
  id;
  product;
  service:ProductsService;
  //Form Builder
 productForm = new FormGroup({
      name:new FormControl(),
      price:new FormControl(),
      id:new FormControl()
  });  

  constructor(service:ProductsService, private formBuilder:FormBuilder, private route: ActivatedRoute, private router: Router) {   	    
    this.service = service;    
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = +params.get('id'); // This line converts id from string into num      
      this.service.getProduct(this.id).subscribe(response=>{
        this.product = response.json();
        console.log(this.product.name);
        this.name = this.product.name
        this.price = this.product.price                                
      });      
    });
  }

   sendData(){
    this.addProduct.emit(this.productForm.value.name);
    this.productForm.value.id = this.id;
    
    this.service.updateProduct(this.productForm.value).subscribe(
      response =>{              
        this.addProductPs.emit();
        this.router.navigate(['/products']);
      }      
    )    
  }


  ngOnInit() {
  }

}
