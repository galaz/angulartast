import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ProductsService } from '../products.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  
  // Variables
  products;
  productsKeys;
  length_keys;

  // Instance Variables
  service:ProductsService;
  //Form Builder
  productForm = new FormGroup({
      name:new FormControl(),
      
  });  

  constructor(service:ProductsService, private router:Router, private formBuilder:FormBuilder) { 
  	this.service = service;
    
  }

 
	ngOnInit() {
		          
  }    

}
