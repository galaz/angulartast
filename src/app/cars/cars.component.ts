import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"; //Login without JWT

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  //Login without JWT
  constructor(private router:Router) { }

  ngOnInit() {
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      //this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }
  }

}
