
import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { Router } from "@angular/router"; //Login without JWT

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  // Variables  
  users;
  usersKeys = [];

  constructor(private service:UsersService, private router:Router) {   
    service.getUsers().subscribe(response=>{      
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
      console.log(response.json());         
  });
   }

   //Login without JWT
  logout(){ 
      localStorage.removeItem('auth');      
      this.router.navigate(['/login']);
  }
  
    deleteUser(key){    
      let index = this.usersKeys.indexOf(key);
      this.usersKeys.splice(index,1);
      this.service.deleteUser(key).subscribe(
        response=> console.log(response));
    }
  
    updateUser(id){
      this.service.getUser(id).subscribe(response=>{
        this.users = response.json();      
      });   
     }
  ngOnInit() {

    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      //this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    } 
  }

}
