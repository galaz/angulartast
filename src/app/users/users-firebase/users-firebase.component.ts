import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { UsersService } from './../users.service';
import { Router } from "@angular/router"; //Login without JWT

@Component({
  selector: 'app-users-firebase',
  templateUrl: './users-firebase.component.html',
  styleUrls: ['./users-firebase.component.css']
})
export class UsersFirebaseComponent implements OnInit {

  // Variables	  
  users;
  usersKeys = [];

  constructor(private service:UsersService,  private router:Router) {  	
  	service.getUsersFireBase().subscribe(response=>{ 
      console.log(response);      		
    	this.usersKeys = Object.values(response);    	
 	  });
  }

  ngOnInit(){
    //Login without JWT
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
     // this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }  
  }

}