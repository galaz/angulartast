import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
  name;
  phone;

  // Adding emitters
  @Output() addUser:EventEmitter <any> = new EventEmitter <any>();
  @Output() addUserPs:EventEmitter <any> = new EventEmitter <any>();
  // Instance Variables
  id;
  user;
  service:UsersService;
  //Form Builder
  userForm = new FormGroup({
      name:new FormControl(),
      phone:new FormControl(),
      id:new FormControl()
  });  

  constructor(service:UsersService, private formBuilder:FormBuilder, private route: ActivatedRoute, private router: Router) {   	    
    this.service = service;    
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = +params.get('id'); // This line converts id from string into num      
      this.service.getUser(this.id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user.name);
        this.name = this.user.name
        this.phone = this.user.phone                                
      });      
    });
  }

   sendData(){
    this.addUser.emit(this.userForm.value.name);

    console.log(this.name.value);
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.updateUser(this.userForm.value,id).subscribe(
        response => {
          console.log(response.json());
          this.addUserPs.emit();
          this.router.navigateByUrl("/")//חזרה לדף הבית
        }
      );
    })
    
  }
 
    
     ngOnInit() {
     this.route.paramMap.subscribe(params=>{
       let id = params.get('id');
       console.log(id);
       this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
       console.log(this.user);
       })
   })

    //Login without JWT
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      //this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }  
   }
  

}