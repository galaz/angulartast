
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UsersService } from '../users.service';
import { Router } from "@angular/router"; //Login without JWT

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {

  // Adding emitters
  @Output() addUser:EventEmitter <any> = new EventEmitter <any>();
  @Output() addUserPs:EventEmitter <any> = new EventEmitter <any>();
  // Instance Variables
  service:UsersService;
  //Form Builder
  userForm = new FormGroup({
      name:new FormControl(),
      phone:new FormControl(),
  });  

  constructor(service:UsersService, private formBuilder:FormBuilder, private router:Router) { 
  	this.service = service;
    console.log('From Constructed');
  }

  sendData(){
    this.addUser.emit(this.userForm.value.name);
    this.service.postUser(this.userForm.value).subscribe(
      response =>{        
        console.log(response)
        this.addUserPs.emit();
      }
    )
  }

	ngOnInit() {
		this.userForm = this.formBuilder.group({
	      name:  [null, [Validators.required]],
	      phone: [null, Validators.required],
      });
          
      //Login without JWT
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
     // this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    } 
  }    

}